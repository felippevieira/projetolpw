﻿<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="validarEmail.js"></script>
  <title>Login</title>
</head>
<body>
  <div class="p-3 mb-2 bg-info text-dark">
    <div class="jumbotron d-flex align-items-center">
      <div class="container">
        <div class="col-sm-4">
          <h5 class="card-title">Tela Login</h5>
          <form action="controller.php" method="POST" name="formInicial">
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entre com email">
            </div>
            <div id="msgemail"></div>
            <div class="form-group">
              <label for="exampleInputPassword1">Senha</label>
              <input type="password" name="senha1" class="form-control" id="exampleInputPassword1" placeholder="Senha">
            </div>
            <a href="cadastroCliente.html" class="badge badge-light">Quero ser cliente do Serviço Fácil</a>
            <a href="cadastroPrestador.html" class="badge badge-light">Sou profissional e quero me candidatar a prestar serviços</a>
            <button type="submit" class="btn btn-primary" style="margin-top: 2em;" onclick="validarEmail(formInicial.email)">Enviar</button>
          </form>
        </div>
      </div>
      <img src="images/img01.gif" class="rounded float-left" alt="Faetec Logo">
    </div>
  </div>
</body>
</html>
